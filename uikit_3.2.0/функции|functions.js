UIkit_установить = function(мир, имяМодуля)
{
    var модуль = мир.модули.модульПоИмени(имяМодуля);

    var css = модуль.содержимое["/uikit_3.2.0/uikit.min.css"];
    var js = модуль.содержимое["/uikit_3.2.0/uikit.min.js"];
    var iconsJS = модуль.содержимое["/uikit_3.2.0/uikit-icons.min.js"];

    // Применить стиль.
    var style = document.createElement("style");
    document.head.appendChild(style);
    style.innerHTML = css;

    function загрузитьСкрипт(код)
    {
        var скрипт = document.createElement("script");
        скрипт.innerHTML = код;
        document.body.appendChild(скрипт);
    }

    загрузитьСкрипт(js);
    загрузитьСкрипт(iconsJS);
};

UIkit_install = UIkit_установить;
